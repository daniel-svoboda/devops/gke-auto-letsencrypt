### Ensure Helm is setup
[helm-setup](https://gitlab.com/daniel-svoboda/devops/helm-setup)

## Steps to follow:
- create namespace letsencrypt
  - `kubectl create namespace letsencrypt`
- lookup values.yaml and add/amend any values required
- setup cert-manage
  - `helm install --name cert-manager --namespace letsencrypt stable/cert-manager --set createCustomResource=false`
  - `helm upgrade --install cert-manager stable/cert-manager --namespace letsencrypt -f values.yaml`
- create ClusterIssuer
  - `echo "
    apiVersion: certmanager.k8s.io/v1alpha1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt
    spec:
      acme:
        server: https://acme-v02.api.letsencrypt.org/directory
        email: <specify-email>
        privateKeySecretRef:
          name: letsencrypt
        http01: {}" | kubectl create -f -`

# verify installation
- `kubectl get crd`
- `helm ls --all cert-manager`

# Cleanup:
- `helm del --purge cert-manager`


# NOTES:
- normally a single helm install cmd is enough, latest version introduced bug, so split in 2 cmds is required


## Test for Konghq implementation:
- `helm upgrade --install cert-manager stable/cert-manager --namespace letsencrypt --set createCustomResource=true --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer --set podAnnotations={configuration.konghq.com: "kong-letsencrypt-ingress"}`

